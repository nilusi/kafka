package com.example.kafkaconsumer.service;

import static sbe.persistence.MessageHeaderDecoder.ENCODED_LENGTH;

import java.nio.ByteBuffer;

import org.agrona.MutableDirectBuffer;
import org.agrona.concurrent.UnsafeBuffer;
import org.springframework.stereotype.Component;

import sbe.persistence.MessageHeaderDecoder;
import sbe.persistence.MessageHeaderEncoder;
import sbe.persistence.TradeDecoder;
import sbe.persistence.TradeEncoder;
import sbe.persistence.TradeType;

@Component
public class DecodingService {

	private static final TradeDecoder tradeDecoder = new TradeDecoder();
	private static final MessageHeaderDecoder messageHeaderDecoder = new MessageHeaderDecoder();

	public void readTrade(ByteBuffer buffer) {

		MutableDirectBuffer directBuffer = new UnsafeBuffer(buffer);

		messageHeaderDecoder.wrap(directBuffer, 0);
		tradeDecoder.wrap(directBuffer, ENCODED_LENGTH, messageHeaderDecoder.blockLength(),
				messageHeaderDecoder.version());
		System.out.println(tradeDecoder);

	}

}
