package com.example.kafkaconsumer.service;

import java.nio.ByteBuffer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Service
public class ConsumerService {

	@Autowired
	DecodingService decodingService;
	
	@KafkaListener(topics = "kafka-spring-producer", group = "group_json", containerFactory = "sbeKafkaListenerFactory")
	public void receive(@Payload ByteBuffer byteBuffer,
			@Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition) {
		
		decodingService.readTrade(byteBuffer);
		System.out.print("Successfully receieved");
	}
}
