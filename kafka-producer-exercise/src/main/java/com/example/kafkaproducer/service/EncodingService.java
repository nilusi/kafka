package com.example.kafkaproducer.service;

import java.nio.ByteBuffer;

import org.agrona.MutableDirectBuffer;
import org.agrona.concurrent.UnsafeBuffer;
import org.springframework.stereotype.Component;

import sbe.persistence.MessageHeaderEncoder;
import sbe.persistence.TradeEncoder;
import sbe.persistence.TradeType;

@Component
public class EncodingService {

	private static final TradeEncoder tradeEncoder = new TradeEncoder();
	private static final MessageHeaderEncoder messageHeaderEncoder = new MessageHeaderEncoder();


	public  TradeEncoder writeTrade(ByteBuffer buffer) {

		MutableDirectBuffer mutableBuffer = new UnsafeBuffer(buffer);
		tradeEncoder.wrapAndApplyHeader(mutableBuffer, 0, messageHeaderEncoder);
		tradeEncoder.tradeId(1).customerId(999).tradeType(TradeType.Buy).qty(100).symbol("GOOG").exchange("NYSE");
		System.out.println(tradeEncoder);

		return tradeEncoder;
	}

	
}
