/* Generated SBE (Simple Binary Encoding) message codec */
package sbe.persistence;

@javax.annotation.Generated(value = { "uk.co.real_logic.sbe.generation.java.JavaGenerator" })
public enum TradeType
{
    Buy((short)0),
    Sell((short)1),
    NULL_VAL((short)255);

    private final short value;

    TradeType(final short value)
    {
        this.value = value;
    }

    public short value()
    {
        return value;
    }

    public static TradeType get(final short value)
    {
        switch (value)
        {
            case 0: return Buy;
            case 1: return Sell;
        }

        if ((short)255 == value)
        {
            return NULL_VAL;
        }

        throw new IllegalArgumentException("Unknown value: " + value);
    }
}
