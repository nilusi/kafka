package com.example.kafkaproducer.controller;

import java.io.IOException;
import java.nio.ByteBuffer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.kafkaproducer.service.EncodingService;

import sbe.persistence.TradeEncoder;

@RestController
public class InitiatorCtrl {


    @Autowired
    private KafkaTemplate<String,ByteBuffer> kafkaTemplate;

    private static final String TOPIC = "kafka-spring-producer";
    
    @Autowired
    EncodingService encodingService;
    
    @GetMapping("/init/{name}")
    public String postMessage(@PathVariable("name") final String name) throws IOException{
       
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(100);
        TradeEncoder tradeEncoder = encodingService.writeTrade(byteBuffer);
       
         System.out.println(byteBuffer);

        kafkaTemplate.send(TOPIC, byteBuffer);
        System.out.printf("Sending message {}" , byteBuffer);
        return "Message Published Successfully";
    }
}
