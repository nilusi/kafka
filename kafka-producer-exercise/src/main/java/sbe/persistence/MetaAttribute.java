/* Generated SBE (Simple Binary Encoding) message codec */
package sbe.persistence;

@javax.annotation.Generated(value = { "uk.co.real_logic.sbe.generation.java.JavaGenerator" })
public enum MetaAttribute
{
    EPOCH,
    TIME_UNIT,
    SEMANTIC_TYPE,
    PRESENCE
}
